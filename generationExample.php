<?php

include("FloNET.php");

$FloNET = new FloNET();
$nets = $FloNET->NetGenerator->generateFreshNETs(250,2,1,2,20);

$dataSets = array();
$dataSets[] = array("INPUT" => array("INPUT1" => 1, "INPUT2" => 1), "OUTPUT" => array("OUTPUT1" => 0));
$dataSets[] = array("INPUT" => array("INPUT1" => 0, "INPUT2" => 0), "OUTPUT" => array("OUTPUT1" => 0));
$dataSets[] = array("INPUT" => array("INPUT1" => 1, "INPUT2" => 0), "OUTPUT" => array("OUTPUT1" => 1));
$dataSets[] = array("INPUT" => array("INPUT1" => 0, "INPUT2" => 1), "OUTPUT" => array("OUTPUT1" => 1));

$FloNET->GenerationHandler->startEvolution($nets,$dataSets);