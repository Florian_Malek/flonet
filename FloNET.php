<?php

include_once("FloDB/FloDB.php");
include_once("NetGenerator.php");
include_once("NetTester.php");
include_once("NetMutation.php");
include_once("GenerationStuff/GenerationHandler.php");

class FloNET{

    public $FloDB;
    public $NetGenerator;
    public $NetTester;
    public $NetMutation;
    public $GenerationHandler;

    function __construct(){
        $this->FloDB = new FloDB();
        $this->NetGenerator = new NetGenerator();
        $this->NetTester = new NetTester();
        $this->NetMutation = new NetMutation();
        $this->GenerationHandler = new GenerationHandler();
    }

    function test(){
        return $this->NetTester->useNets($this->NetGenerator->generateFreshNETs(4,2, 1, 6, 15,"testNET", 0.5),array("INPUT1" => 1, "INPUT2" => 0));
        //return $this->NetTester->useNets($this->FloDB->pullRecursive("testNET"),array("INPUT1" => 1, "INPUT2" => 0));
    }

}


/*
$FloNET = new FloNET();
$path = "blablubblaaaa";
print_r($FloNET->test());
*/
//print_r($testNeuron);

//print_r($test);
//print_r($FloNET->makeNeuron(array("N1" => "hallo","N2"=>"na","N3"=>"du","N4"=>"hehe")));
