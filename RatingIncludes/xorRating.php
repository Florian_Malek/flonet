<?php

function rateNET($testRuns){
    $netRatings = 0;
    foreach ($testRuns as $testRun) {
        foreach ($testRun["XPECTED"] as $outputKey => $output) {
            if($output == 1 && $testRun["GOTTEN"][$outputKey] == 0){
                $netRatings += 15000;
            }else{
                $netRatings += abs($output - $testRun["GOTTEN"][$outputKey]);
            }
        }
    }
    return $netRatings;
}

?>