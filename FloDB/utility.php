<?php

function getLastPathEntity($path){
    $pathArray = explode("/",$path);
    return $pathArray[count($pathArray) - 1];
}

function getLastID($xmlFile){
        $xml = simplexml_load_file($xmlFile);			
        $id = (array)$xml->xpath("//lastInsertID")[0];
        return $id[0];
}

function endsWith($haystack, $needle)
{
    $length = strlen($needle);
    if ($length == 0) {
        return true;
    }

    return (substr($haystack, -$length) === $needle);
}
?>