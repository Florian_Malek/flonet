<?php

include_once("utility.php");

class Writer{

function array2XML($array){
        $xml = "";
        foreach ($array as $key => $value) {
                if(gettype($value) == "array"){
                        $value = $this->array2XML($value);
                }else{
                        $value = str_replace("<"," ",$value);
                }
                $testkey = substr($key, 0, 1);
                if(is_numeric($testkey)){
                        $key = '_'.$key;
                }
                
                $xml = $xml.'<'.$key.">".$value."</".$key.'>';
        }
        return $xml;
}

function setInTag($tag,$xmlString,$data){
        $after = strstr($xmlString, "</".$tag);
        $before = strstr($xmlString, "<".$tag,true)."<".$tag.">";
        return $before.$data.$after;
}


function addInTag($tag,$xmlString,$data){

        $before = strstr($xmlString, "</".$tag,true);
        $after = strstr($xmlString, "</".$tag);
        return $before.$data.$after;
}

function replaceByID($xmlString,$xmlData,$id){

        $beforeID = strstr($xmlString,'<id>'.$id.'</id>',true);
        $after = strstr($xmlString, '<id>'.$id.'</id>');
        $pos = strripos($beforeID, '<item>');
        $before = substr($beforeID,0,$pos).'<item>
                ';

        return $before.$xmlData.$after;
}

function overwriteEntry($path,$id,$data){

        $xmlFile= $path.".xml";
        $xmlString = file_get_contents($xmlFile);
        $xmlData = $this->array2XML($data);

        if(stristr($xmlString, '<id>'.$id.'</id>') === FALSE) {
                echo 'Entry with id = '.$id.' not found.';
                return false;
        }

        $finalString = $this->replaceByID($xmlString,$xmlData,$id);

        $file = fopen($xmlFile, "wa+");
        fwrite($file, $finalString);
        fclose($file);

}

function deleteByID($path, $id){
        $xmlFile= $path.".xml";
        $xmlString = file_get_contents($xmlFile);

        if(stristr($xmlString, '<id>'.$id.'</id>') === FALSE) {
                echo 'Entry with id = '.$id.' not found.';
                return false;
        }

        $beforeID = strstr($xmlString,'<id>'.$id.'</id>',true);
        $afterID = strstr($xmlString, '<id>'.$id.'</id>');
        $after = strstr($afterID, '</item>');
        $after = substr($after, 7);
        $pos = strripos($beforeID, '<item>');
        $before = substr($beforeID,0,$pos);


        $finalString = $before.$after;
        
        $file = fopen($xmlFile, "wa+");
        fwrite($file, $finalString);
        fclose($file);
}

function deleteDB($path){
        if(is_file($path.".xml")){
                unlink($path.".xml");
        }
}


function save($data, $path){

        
        $id = getLastID($path.".xml") + 1;

        $data["id"] = $id;
        $xmlFile= $path.".xml";
        $xmlString = file_get_contents($xmlFile);

        $data = '       <item>
                '.$this->array2XML($data).'
        </item>
';

        $tag = getLastPathEntity($path);

        $finalString = $this->addInTag($tag,$xmlString, $data);
        $finalString = $this->setInTag("lastInsertID",$finalString,$id);

        $file = fopen($xmlFile, "wa+");
        fwrite($file, $finalString);
        fclose($file);
}

function buildDB($path){
        $pathArray = explode("/",$path);
        $filename = $pathArray[count($pathArray) - 1];

        $dir = '';
        $i = 0;
        while ($i < count($pathArray)-1) {
                $dir = $dir.$pathArray[$i]."\\";
                $i++;
            }
        if(!is_dir($dir)){
                mkdir($dir, 0777,true);
        }
        $file = fopen($dir.$filename.'.xml',"wa+");
        fwrite($file,'<?xml version="1.0" encoding="UTF-8"?>
<'.$filename.'>
	<lastInsertID>0</lastInsertID>
</'.$filename.'>');
}

function deleteDBHard($dirname)
{   if(is_dir($dirname)) $dir_handle = opendir($dirname);
 
    //Falls Verzeichnis nicht geoeffnet werden kann, mit Fehlermeldung terminieren
    if(!$dir_handle)
    { echo "Verzeichnis nicht gfunden! ({$dirname})";
      return  false;
    }
 
    while($file=readdir($dir_handle))
    {  if($file!="." && $file!="..")
        {  if(!is_dir($dirname."/".$file))
            { //Datei loeschen 
              @unlink($dirname."/".$file);
            }
            else
            { //Falls es sich um ein Verzeichnis handelt, "delete_directory" aufrufen
              $this->deleteDBHard($dirname.'/'.$file);
            }
          }
    }
 
    closedir($dir_handle);
    //Verzeichnis loeschen
    rmdir($dirname);
    return  true;
}


}

?>