<?php
include_once("writer.php");
include_once("reader.php");
include_once("utility.php");


class FloDB{

public $reader;
public $writer;
public $pathToXML;

function __construct(){
    $this->reader = new Reader();
    $this->writer = new Writer();
    $this->pathToXML = __DIR__."/../data/";
}

function push($data, $path){

    $pathArray = explode("/",$path);
    $lastPart = $pathArray[count($pathArray) - 1];

        if(!is_numeric($lastPart)){
            $path = $this->pathToXML.$path;
            $path = str_replace('\\','/',$path);
            if(!is_file($path.".xml")){
                //setze neue Datenbank auf
                $this->writer->buildDB($path);
            }
            //speichere Eintrag
            return $this->writer->save($data,$path);
            $xmlFileName = $lastPart;
        }else{
            //spezifischer Eintrag nach ID vornehmen
            $id = $lastPart;
            $xmlFileName = $pathArray[count($pathArray) - 2];
            $i = 0;
            $path = $this->pathToXML;
            while ($i < count($pathArray)-1) {
                $path = $path.$pathArray[$i]."\\";
                $i++;
            }
            $path = substr($path, 0, -1);
            $path = str_replace('\\','/',$path);
            if(!is_file($path.".xml")){
                echo "No Database found at that location";
                return false;
            }else{
                //ersetze Eintrag
                unset($data["id"]);
                $this->writer->overwriteEntry($path,$id,$data);
            }
}}

function pull($path){
    $pathArray = explode("/",$path);
    $lastPart = $pathArray[count($pathArray) - 1];

        if(!is_numeric($lastPart)){
            $path = $this->pathToXML.$path;
            $path = str_replace('\\','/',$path);
            if(!is_file($path.".xml")){
                echo "No Database found at that location";
                return false;
            }
            $xmlFileName = $lastPart;
            return $this->reader->read($path);
        }else{
            $xmlFileName = $pathArray[count($pathArray) - 2];
            $i = 0;
            $path = $this->pathToXML;
            while ($i < count($pathArray)-1) {
                $path = $path.$pathArray[$i]."\\";
                $i++;
            }
        $path = substr($path, 0, -1);
        $path = str_replace('\\','/',$path);
            if(!is_file($path.".xml")){
                echo "No Database found at that location";
                return false;
            }
        return $this->reader->read($path, $lastPart)[0];
        }
    
}

function getLastInsertID($path){
    $path = $this->pathToXML.$path.".xml";
    return getLastID($path);
}

function delete($path){
    $pathArray = explode("/",$path);
    $lastPart = $pathArray[count($pathArray) - 1];

    if(!is_numeric($lastPart)){
        echo 'path to entry expected, path to Database given. Maybe you where looking for "deleteDB($path)"? ';
        return false;
    }else{
        $id = $lastPart;
        $i = 0;
        $path = $this->pathToXML;
        while ($i < count($pathArray)-1) {
            $path = $path.$pathArray[$i]."\\";
            $i++;
        }

        $path = substr($path, 0, -1);
        $path = str_replace('\\','/',$path);
    
        if(!is_file($path.".xml")){
            echo "No Database found at that location";
            return false;
        }

        return $this->writer->deleteByID($path, $id);
    }


}

function deleteDB($path){
    $pathArray = explode("/",$path);
    $lastPart = $pathArray[count($pathArray) - 1];

    if(is_numeric($lastPart)){
        echo 'path to Database expected, path to entry given. Maybe you where looking for "delete($path)"? ';
        return false;
    }
    $path = $this->pathToXML.$path;
    $this->writer->deleteDB($path);
}

function deleteDBHard($path){
    $pathArray = explode("/",$path);
    $lastPart = $pathArray[count($pathArray) - 1];

    if(is_numeric($lastPart)){
        echo 'path to Database expected, path to entry given. Maybe you where looking for "delete($path)"? ';
        return false;
    }
    $path = $this->pathToXML.$path;
    $this->writer->deleteDBHard($path);
}

function pullRecursive($path){
    $pathToFolder = $this->pathToXML.$path;
        if(!is_dir($pathToFolder)){
            return $this->pull("path");
        }
        if(is_file($pathToFolder."/../".getLastPathEntity($path).".xml")){
            $returnArray = $this->pull($path."/../".getLastPathEntity($path));
        }else{
            $returnArray = array();
        }

    $allFiles = scandir($pathToFolder);
    //print_r($allFiles);
    foreach ($allFiles as $file) {
        

        if (strpos($file, '.') !== false) {
            if(endsWith($file,'.xml')){
                $xmlFile = substr($file,0, -4);
                $fileArray = $this->pull($path."/".$xmlFile);
                if(isset($returnArray[$xmlFile])){
                    $mergedArray = array_merge($returnArray[$xmlFile], $fileArray);
                    $returnArray[$xmlFile] = $mergedArray;
                }else{
                    $returnArray[$xmlFile] = $fileArray;
                }

                
            }
                
            }else{
                $ordner = $file;

                $ordnerArray = $this->pullRecursive($path."/".$ordner);
                if(isset($returnArray[$ordner])){
                    $mergedArray = array_merge($returnArray[$ordner], $ordnerArray);
                    print_r($mergedArray);
                    $returnArray[$ordner] = $mergedArray;
                }else{
                    $returnArray[$ordner] = $ordnerArray;
                }
                
        }
    }
    //print_r(scandir($path));
    return $returnArray;

}



}



?>