<?php

include_once("utility.php");

class Reader
{


public function read($path,$id = "*"){
	$xml = simplexml_load_file($path.".xml");
	$tag = getLastPathEntity($path);
	$items = $xml->xpath("/".$tag."/item[id=".$id."]");
	return $this->convertSimpleXML($items);	
}

function convertSimpleXML(array $array)
{
	$convertToNonAssoc = false;
	foreach($array as $key => &$value){
		if(gettype($value) == "object"){
			$type = get_class($value);
			if($type == "SimpleXMLElement"){
				$value = (array)$value;
				$value = $this->convertSimpleXML($value);
			}
		}

		//falls ein array nicht assoziativ oder mit einer Zahl am Anfang des Keys abgespeichert wurde, müssen hier die xml-freundlichen tags wieder zurückgewandelt werden
		$testkey = substr($key, 1, 1);
		
		if(is_numeric($testkey)){
			$newKey = substr($key, 1);
			$array[$newKey] = $array[$key];
			unset($array[$key]);
		}
	}

	//die ID wieder an das Ende des Array Setzen
	if(isset($array["id"])){
	$tempID = $array["id"];
	unset($array["id"]);
	$array["id"] = $tempID;
	}
	return $array;
}
}


?>
