<?php

include_once("FloDB/FloDB.php");
include_once("RandUtility.php");

class NetGenerator{

    public $FloDB;

    function __construct(){
        $this->FloDB = new FloDB();
    }

    function generateFreshNETs($numberNets,$numberInputs, $numberOutputs, $layers, $neuronWidth, $path = false, $connectionProbability = 0.6){
        $allNets = array();
        for ($netID=1; $netID <= $numberNets ; $netID++) {
            $netName = "NET".$netID;
            if($path !== false){
                $netPath = $path."/".$netName;
                $this->makeFreshNET($numberInputs, $numberOutputs, $layers, $neuronWidth,$netPath,$connectionProbability);
            }
            $allNets[$netName] = $this->makeFreshNET($numberInputs, $numberOutputs, $layers, $neuronWidth,false,$connectionProbability);
        }
        return $allNets;

    }



    function makeFreshNET($numberInputs, $numberOutputs, $layers, $neuronWidth,$path = false,$connectionProbability = 0.6){
        $net = array();
        //InputLayer

        $firstLayer = array();
        for ($f=1; $f <= $numberInputs; $f++) { 
            $name = "INPUT".$f;
            $firstLayer[$name] = array(); 
        }
        $net[] = $firstLayer;
        if($path !== false){
            $this->FloDB->push($firstLayer, $path);
        }
        
        $previousLayer = $firstLayer;

        //Zwischenlayer

        $l = 1;
        for($l = 1; $l <= $layers; $l++) {
            $layer = array();
            for ($n=1; $n <= $neuronWidth; $n++) {
                $neuron = $this->makeNeuron($previousLayer);
                $id = "NEURON".$n;
                //echo $id;
                $layer[$id] = $neuron;
            }

            $net[] = $layer;
            if($path !== false){
                $this->FloDB->push($layer, $path);
            }
            $previousLayer = $layer;
        }

        //EndLayer

        $lastLayer = array();
        for ($f=1; $f <= $numberOutputs; $f++) { 
            $name = "OUTPUT".$f;
            $outputNeuron = $this->makeNeuron($previousLayer);
            $lastLayer[$name] = $outputNeuron; 
        }
        $net[] = $lastLayer;
        if($path !== false){
            $this->FloDB->push($lastLayer, $path);
        }
        return $net;
    }

    function makeOutput($previousLayer, $connectionProbability = 0.6){
        foreach ($previousLayer as $key => $prevNeuron) {
            if(YesOrNo($connectionProbability)){
                $connectedNeurons[] = $key;
            }
        }
        $outputNeuron = array("connectedNeurons" => $connectedNeurons);
        return $outputNeuron;
        
    }

    function makeNeuron($previousLayer, $minMAXX = 0.1, $maxMAXX = 5, $minOUTX = 0, $maxOUTX = 2, $connectionProbability = 0.5){

        $maxX = randomNumber($minMAXX, $maxMAXX);
        $outX = randomNumber($minOUTX, $maxOUTX);

        $connectedNeurons = array();


        foreach ($previousLayer as $key => $prevNeuron) {
            if(YesOrNo($connectionProbability)){
                $connectedNeurons[] = $key;
            }
        }


        $neuron = array('maxX'=> $maxX, 'outX' => $outX, 'connectedNeurons' => $connectedNeurons);
        return $neuron;
    }

    
}

