<?php

function YesOrNo($probability = 0.5){
        $random = rand(0,100)*0.01;
        return $random <= $probability;
    }

    function randomNumber($min,$max, $digits=3){
        $factor = pow(10,$digits);
        $randomNumber = rand($min*$factor, $max*$factor);
        return $randomNumber / $factor;
    }

    function PosOrNeg($probability = 0.5){
        if(YesOrNo($probability)){
            return 1;
        }else{
            return -1;
        }
    }

    ?>