<?php

class GenerationHandler{

function startString(){
    return '<?php include_once("GenerationStuff/GenerationHandler.php"); $generationHandler = new GenerationHandler();
    ';
}

function linkToNext($link){
    return '<script>
window.location.href = "'.$link.'";
</script>';
}


function writeFile($fileName, $string){

    $myfile = fopen($fileName, "w") or die("Unable to open file!");
    fwrite($myfile, $string);
    fclose($myfile);

}

function declareID($ID){
    return '$ID = '.$ID.';
    $nextID = $ID + 1;';
}

function echoFunction($string){
    return 'echo '.$string.';';
}

function linkToNextIf($fileName){
    return 'if(is_dir("deleteToStop")){
    echo linkToNext("'.$fileName.'");
}';
}

function unlinkFunction($path){
return 'unlink("'.$path.'");';
}



function writeNextGen($ID){
    $fileName = "Generation".$ID.".php";
        $nextID = $ID +1;
    $nextFileName = "Generation".$nextID.".php";
    $string = $this->startString().$this->declareID($ID);
    $string .= ' include("GenerationStuff/forEachGeneration.php");';
   $string .= ' include("GenerationStuff/GenerationIterator.php");';
    $this->writeFile($fileName,$string);
}

function startEvolution($nets,$trainingData){
    session_start();
    $_SESSION["nets"] = $nets;
    $_SESSION["trainingData"] = $trainingData;
    mkdir("deleteToStop");
    $ID = 1;
    $fileName = "Generation".$ID.".php";

    $this->writeNextGen($ID);
    echo '<script>
window.location.href = "'.$fileName.'";
</script>';
}


}



?>