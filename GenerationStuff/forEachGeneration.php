<?php

session_start();
$nets = $_SESSION["nets"];

include_once("FloNET.php");
$FloNET = new FloNET();

$stepSize = 0.1;
$mutationProbability = 0.5;


$nets = $_SESSION["nets"];
$dataSets = $_SESSION["trainingData"];

$bewertungen = $FloNET->NetTester->rateNets($nets, $dataSets, "xorRating.php");
asort($bewertungen);

$numberNets = sizeof($bewertungen);
$keys = array_keys($bewertungen);

$i = 0;
foreach ($bewertungen as $netID => $value) {
    if($i<0.5*$numberNets){
        $toBeKilledNetNumber = $i + 0.5*$numberNets;
        $nets[$keys[$toBeKilledNetNumber]] = $FloNET->NetMutation->mutateNET($nets[$keys[$i]], $stepSize, $mutationProbability);
    }
    $i++;
}/*
echo "momentane kleinste Verfehlung: ";
print_r($bewertungen[$keys[0]]);
echo "</br>momentan größte Verfehlung: ";
print_r($bewertungen[$keys[$numberNets-2]]);
*/
$debug = array();
foreach ($keys as $keykey => $key) {
    $debug[$keykey] = $bewertungen[$key];
}

print_r($debug);

echo "</br></br>Generation: ";

$_SESSION["nets"] = $nets;


?>