<?php

include_once("../FloNET.php");

$FloNET = new FloNET();

$bewertungen = json_decode($_POST["ratings"],true);

$stepSize = $_POST["stepSize"];
$mutationProbability = $_POST["mutationProbability"];


$numberNets = sizeof($bewertungen);

session_start();
$nets = $_SESSION["nets"];



$i = 0;
asort($bewertungen);
$bewertungen = array_reverse($bewertungen);
$keys = array_keys($bewertungen);

foreach ($bewertungen as $netID => $value) {
    if($i<0.5*$numberNets){
        $toBeKilledNetNumber = $i + 0.5*$numberNets;
        $nets[$keys[$toBeKilledNetNumber]] = $FloNET->NetMutation->mutateNET($nets[$keys[$i]], $stepSize, $mutationProbability);
    }
    $i++;
}


//add random generated nets
$numberOfNewlyGeneratedNets = 2;

for($i = 1; $i <= $numberOfNewlyGeneratedNets; $i++){
    $nets[$keys[$numberNets - $i]] = $FloNET->NetGenerator->makeFreshNET($_SESSION["numberInputs"], $_SESSION["numberOutputs"], $_SESSION["layers"], $_SESSION["neuronWidth"]);
}


$_SESSION["nets"] = $nets;
echo "done";


?>