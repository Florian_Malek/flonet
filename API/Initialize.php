<?php

include_once("../FloNET.php");

$FloNET = new FloNET();


$numberNets = $_POST["numberNets"];
$numberInputs = $_POST["numberInputs"];
$numberOutputs = $_POST["numberOutputs"];
$layers = $_POST["layers"];
$neuronWidth = $_POST["neuronWidth"];


session_start();
$_SESSION["nets"] = $FloNET->NetGenerator->generateFreshNETs($numberNets,$numberInputs, $numberOutputs, $layers, $neuronWidth, $path = false, $connectionProbability = 0.6);
$_SESSION["numberInputs"] = $numberInputs;
$_SESSION["numberOutputs"] = $numberOutputs;
$_SESSION["layers"] = $layers;
$_SESSION["neuronWidth"] = $neuronWidth;

echo "done";
?>