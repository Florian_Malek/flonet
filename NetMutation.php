<?php

include_once("FloDB/FloDB.php");
include_once("RandUtility.php");

class NetMutation{

    public $FloDB;

    function __construct(){
        $this->FloDB = new FloDB();
    }

    function mutateNET($net, $stepSize, $mutationProbability){
        $mutatedNET = array();
        $previousLayer = $net[0];
        $mutatedNET[] = $previousLayer;

        foreach ($net as $layerKey => $layer) {
            if($layerKey !=  0){
                $mutatedLayer = array();
                foreach ($layer as $key => $neuron) {
                    if($key != "id"){
                        if(YesOrNo($mutationProbability)){
                            $neuron = $this->mutateNeuron($previousLayer, $neuron, $stepSize);
                        }
                        $mutatedLayer[$key] = $neuron;
                    }
                }  
            $mutatedNET[$layerKey] = $mutatedLayer;
            $previousLayer = $mutatedLayer;
            }
        }
        return $mutatedNET;
    }

    function mutateNeuron($previousLayer, $neuron, $stepSize){

        $maxX = $neuron["maxX"];
        $outX = $neuron["outX"];

        $maxX += PosOrNeg() * $stepSize;
        $outX += PosOrNeg() * $stepSize;

        $connectedNeurons = array();
        $maxNeurons = sizeof($previousLayer);
        $currentNeurons = sizeof($neuron["connectedNeurons"]);
        if($currentNeurons == 0){
            $currentNeurons = 0.5* $maxNeurons;
        }
        $connectionProbability = $currentNeurons/$maxNeurons;

        foreach ($previousLayer as $key => $prevNeuron) {
            if(YesOrNo($connectionProbability)){
                if($key != "id"){
                $connectedNeurons[] = $key;
                }
            }
        }


        $mutatedNeuron = array('maxX'=> $maxX, 'outX' => $outX, 'connectedNeurons' => $connectedNeurons);
        return $mutatedNeuron;
    }


}