<?php

include_once("FloDB/FloDB.php");

class NetTester{

    public $FloDB;

    function __construct(){
        $this->FloDB = new FloDB();
    }

    function useNets($nets,$input){

        $alleErgebnisse = array();

        foreach ($nets as $netID => $net) {
            $netErgebnis = $this->useNet($net, $input[$netID]);
            $alleErgebnisse[$netID] = $netErgebnis;
        }
        return $alleErgebnisse;
    }



    function useNet($net,$input){

        //gibt momentan alle Ergebnisse zurück, nur die der Outputs wäre sparsamer

        $ergebnisse = array();
        $numberLayers = sizeof($net);

        $vorherigeErgebnisse = $input;


        foreach ($net as $layerKey => $layer) {
            
            if($layerKey !=  0){
                //echo "</br></br>layer: ".$key."</br></br>";
            $layerErgebnisse = array();
            foreach ($layer as $key => $neuron) {
                if($key != "id"){
                $summe = 0;
                if(sizeof($neuron["connectedNeurons"]) == 0){
                    $summe = 0;
                }else{
                    foreach ($neuron["connectedNeurons"] as $neuron2) {
                        $summe += $vorherigeErgebnisse[$neuron2];
                            //echo $neuron2."</br>";
                            //echo $vorherigeErgebnisse[$neuron2]."</br>";

                    }
                    //echo "summe:".$summe."</br>";
                }
                    //echo " Summe: ".$summe;
                    //echo " maxX: ".$neuron["maxX"]." outX: ".$neuron["outX"]."</br>";

                if($summe > $neuron["maxX"]){
                    $neuronErgebnis = floor($summe/$neuron["maxX"])*$neuron["outX"];
                }else{
                    $neuronErgebnis = 0;
                }
                    //echo "neuronErgebnis: ".$neuronErgebnis."</br></br>";
                    //echo $key.": ".$neuronErgebnis."</br>";
                $layerErgebnisse[$key] = $neuronErgebnis;
            }}

            $ergebnisse[$layerKey] = $layerErgebnisse;
            $vorherigeErgebnisse = $layerErgebnisse; 
            }
        }
        return $ergebnisse;
    }

    function rateNets($nets, $dataSets, $ratingAlgorithmPath){
        include_once("RatingIncludes/".$ratingAlgorithmPath);
        $ratings = array();
        foreach ($nets as $netID => $net) {
            $testRuns = array();
            foreach ($dataSets as $dataSetID => $dataSet) {
                $inputs = array();
                foreach ($dataSet["INPUT"] as $inputKey => $SetInput) {
                    $inputs[$inputKey] = $SetInput;
                }
                $ergebnis = $this->useNet($net, $inputs);
                $xpectedAndGottenResults = array( "XPECTED" => $dataSet["OUTPUT"], "GOTTEN" => $ergebnis[sizeof($ergebnis)]);
                $testRuns[$dataSetID] = $xpectedAndGottenResults;
            }
            //      [TestlaufID][XPECTED/GOTTEN][outputName]
            
            $netRating = rateNET($testRuns);
            /*
            print_r($testRuns);
            echo "</br></br>";
            echo $netRating."</br></br>";
            */
            $ratings [$netID] = $netRating;
        }

        return $ratings;
    }

}